/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-29 16:45:53
 */
import '@/assets/css/base.css'
import '@imengyu/vue3-context-menu/lib/vue3-context-menu.css'
import store from '@/store'
import directives from '@/directives'
import ContextMenu from '@imengyu/vue3-context-menu'
import { App } from 'vue'
/** 公共插件*/
const useCommonPlugins = (app: App) => {
  app.use(store)
  app.use(directives)
  app.use(ContextMenu)
}

export default useCommonPlugins
