/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-26 17:51:17
 */
/**获取文件 */
export const useViteFile = (filePath: string) => {
  const url = new URL(`../assets/images/${filePath}`, import.meta.url).href
  return url
}