import { createApp } from 'vue'
import router from '@/router'
import App from './App.vue'
import Plugins from '@/hooks/useCommonPlugins'

createApp(App)
  .use(Plugins)
  .use(router)
  .mount('#app')

