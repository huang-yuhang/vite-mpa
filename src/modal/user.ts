export interface UserModel {
  id: string
  email: string
  username: string
  address: string
  token: string
  avatar:string
}