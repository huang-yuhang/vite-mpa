/*
* @Description: 
* @Author: HYH
* @LastEditors: HYH
* @LastEditTime: 2023-05-27 22:06:46
*/
import { createApp } from 'vue'
import router from './router/index'
import App from './App.vue'
import Plugins from '@/hooks/useCommonPlugins'

createApp(App)
  .use(Plugins)
  .use(router)
  .mount('#daisy')

