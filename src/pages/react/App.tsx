/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:52:28
 */
import React from'react'
import './assets/base.css'
import Layout from '@react/views/layout';
import { useCss } from '@react/hooks/useCss';

const App = () => {
  useCss()
  return <Layout />
}

export default App;