/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-20 14:27:59
 */
const sleep = (time: number) => new Promise((resolve) => setTimeout(resolve, time))

export const getMenu = async () => {
  await sleep(2000)
  return [
    {
      path: 'home',
    },
    {
      path: 'three',
      children: [
        {
          path: 'base',
        },
        {
          path: 'test',
        },
      ]
    },
  ]
}