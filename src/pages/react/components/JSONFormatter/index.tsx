/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-21 14:42:53
 */
import ReactJson, { ReactJsonViewProps } from 'react-json-view'


export default function JSONFormatter(props: ReactJsonViewProps) {
  return (
    <ReactJson
      // 子节点数量
      displayObjectSize={true}
      // 子节点类型.
      displayDataTypes={false}
      theme="google"
      iconStyle="triangle"
      collapseStringsAfterLength={30}
      style={{
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: '7px',
        padding: '10px'
      }}
      indentWidth={2}
      {...props}
    />
  )
}

