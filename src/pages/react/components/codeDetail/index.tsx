/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 10:47:37
 */
import HLJS from "@react/components/highLight";
import { Copy, Instruction } from "@icon-park/react";
import { Tooltip, Tabs, TabsProps, message } from "antd";
import React, { useEffect, useState } from "react";
import _copy from 'copy-to-clipboard';
import style from './index.module.scss'

type IProps = {
  contents: { tabs: string, lang: string, content: string }[]
  children?: JSX.Element
}

const CodeDetail = (props: IProps) => {
  const { contents, children } = props
  const [visible, setVisible] = useState(false)
  const [activeKey, setActiveKey] = useState('')
  const [items, setItems] = useState<TabsProps['items']>([])
  const copy = async () => {
    // @ts-ignore
    const code = items?.filter(item => item.key === activeKey)[0]?.children?.props.content
    _copy(code)
    message.success('复制成功!')
    return
  }
  useEffect(() => {
    const item = contents.map(item => ({ key: item.tabs, label: item.tabs, children: <HLJS content={item.content} lang={item.lang} /> }))
    setItems(item)
    setActiveKey(item[0].key)
  }, [])
  return (
    <div className={style.codeDetail}>
      <div className={style.content}>
        {children}
      </div>
      <div className={style.footer}>
        <div className={style.icons}>
          <Tooltip title="代码">
            <Instruction onClick={() => setVisible(!visible)} theme="outline" size="24" fill="#333" />
          </Tooltip>
          <Tooltip title="复制">
            <Copy onClick={copy} theme="outline" size="20" fill="#333" />
          </Tooltip>
        </div>
      </div>
      <div style={{ display: visible ? "block" : "none" }} className={style.code} >
        <Tabs activeKey={activeKey} items={items} onChange={e => setActiveKey(e)} />
      </div>
    </div>

  )
}


export default CodeDetail