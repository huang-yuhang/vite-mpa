/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-28 16:04:22
 */
import React, { useEffect, useRef } from 'react';
import hljs from 'highlight.js/lib/core';
import 'highlight.js/styles/github.css'; // 样式文件
import javascript from 'highlight.js/lib/languages/javascript'; // 样式文件
import scss from 'highlight.js/lib/languages/scss'; // 样式文件
import typescript from 'highlight.js/lib/languages/typescript'; // 样式文件
hljs.registerLanguage('javascript', javascript);
hljs.registerLanguage('scss', scss);
hljs.registerLanguage('typescript', typescript);


interface IProps {
  content: string
  lang?: string
}


const HLJS = (props: IProps) => {

  const { content, lang } = props

  const codeRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (codeRef.current) {
      codeRef.current.innerHTML = hljs.highlightAuto(content, ([lang || ''] || ['javascript'])).value
    }
  }, []);

  return (
    <pre>
      <code ref={codeRef}>
      </code>
    </pre>
  );

}


export default HLJS