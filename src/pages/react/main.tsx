/*
 * @Description:
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:51:59
 */
import React from'react'
import ReactDOM from 'react-dom/client'
import { HashRouter } from 'react-router-dom'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { globalInject } from '@react/config/globalInject'
import { persistedStore, store } from '@react/store'
import App from './App'

globalInject()
  .then(() => {
    ReactDOM
      .createRoot(document.getElementById('react')!)
      .render(
        <HashRouter>
          <Provider store={store}>
            {/* @ts-ignore */}
            <PersistGate persistor={persistedStore}>
              <App />
            </PersistGate>
          </Provider>
        </HashRouter>
      )

  })
