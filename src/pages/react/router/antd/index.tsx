/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:49:17
 */

import React, { lazy, } from 'react'

import { Appraisal, lazyLoad } from '../'

const WaterMark = lazy(() => import('@react/views/antd/waterMark'))
const App = lazy(() => import('@react/views/antd/app'))
const Tour = lazy(() => import('@react/views/antd/tour'))
const Segmented = lazy(() => import('@react/views/antd/segmented'))
const QrCode = lazy(() => import('@react/views/antd/qrCode'))

export default () => {
  return {
    path: 'antd',
    element: <Appraisal>{lazyLoad('antd')}</Appraisal>,
    children: [
      {
        path: 'waterMark',
        element: <WaterMark />,
        meta: {
          title: '水印',
          parent: 'antd'
        }
      },
      {
        path: 'app',
        element: <App />,
        meta: {
          title: 'app内调用',
          parent: 'antd'
        }
      },
      {
        path: 'tour',
        element: <Tour />,
        meta: {
          title: '引导页',
          parent: 'antd'
        }
      },
      {
        path: 'segmented',
        element: <Segmented />,
        meta: {
          title: 'segmented',
          parent: 'antd'
        }
      },
      {
        path: 'qrCode',
        element: <QrCode />,
        meta: {
          title: '二维码',
          parent: 'antd'
        }
      },
    ]
  }
}