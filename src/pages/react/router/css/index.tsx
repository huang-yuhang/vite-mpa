/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:48:17
 */
import React, { lazy, } from 'react'
import { Appraisal, lazyLoad } from '..'

const BlurBox = lazy(() => import('@react/views/css/components/blurBox'))
const Print = lazy(() => import('@react/views/css/components/print'))
const TextAdjust = lazy(() => import('@react/views/css/components/textAdjust'))
const Progress = lazy(() => import('@react/views/css/components/progress'))
const AudioLoading = lazy(() => import('@react/views/css/components/audioLoading'))
const Filter = lazy(() => import('@react/views/css/components/filter'))
const TextShadow = lazy(() => import('@react/views/css/components/textShadow'))
const WarningLine = lazy(() => import('@react/views/css/components/warningLine'))
const Mesh = lazy(() => import('@react/views/css/components/mesh'))
const Button = lazy(() => import('@react/views/css/components/button'))
const Login = lazy(() => import('@react/views/css/components/Login'))

export default () => {
  return {
    path: 'css',
    element: <Appraisal>{lazyLoad('css')}</Appraisal>,
    children: [

      {
        path: 'blurBox',
        element: <BlurBox />,
        meta: {
          title: '模糊盒子',
          parent: 'css'
        }
      },
      {
        path: 'print',
        element: <Print />,
        meta: {
          title: '打字机效果',
          parent: 'css'
        }
      },
      {
        path: 'textAdjust',
        element: <TextAdjust />,
        meta: {
          title: '文字适应',
          parent: 'css'
        }
      },
      {
        path: 'progress',
        element: <Progress />,
        meta: {
          title: '进度条',
          parent: 'css'
        }
      },
      {
        path: 'audioLoading',
        element: <AudioLoading />,
        meta: {
          title: '音频加载',
          parent: 'css'
        }
      },
      {
        path: 'filter',
        element: <Filter />,
        meta: {
          title: '灰度',
          parent: 'css'
        }
      },
      {
        path: 'textShadow',
        element: <TextShadow />,
        meta: {
          title: '文字阴影',
          parent: 'css'
        }
      },
      {
        path: 'warningLine',
        element: <WarningLine />,
        meta: {
          title: '警戒线',
          parent: 'css'
        }
      },
      {
        path: 'mesh',
        element: <Mesh />,
        meta: {
          title: '网格',
          parent: 'css'
        }
      },
      {
        path: 'button',
        element: <Button />,
        meta: {
          title: '按钮',
          parent: 'css'
        }
      },
      {
        path: 'Login',
        element: <Login />,
        meta: {
          title: '登录',
          parent: 'css'
        }
      },
    ]
  }
}