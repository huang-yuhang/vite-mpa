/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:51:12
 */
import React,{ lazy, } from 'react'
import { Appraisal, lazyLoad } from '..'

const BasicUse = lazy(() => import('@react/views/fabric/basicUse'))
const Custom = lazy(() => import('@react/views/fabric/custom'))
const Test = lazy(() => import('@react/views/fabric/test'))

export default () => {
  return {
    path: 'fabric',
    element: <Appraisal>{lazyLoad('fabric')}</Appraisal>,
    children: [
      {
        path: 'basicUse',
        element: <BasicUse />,
        meta: {
          title: '基本使用',
          parent: 'fabric'
        }
      },
      {
        path: 'custom',
        element: <Custom />,
        meta: {
          title: '自定义',
          parent: 'fabric'
        }
      },
      {
        path: 'test',
        element: <Test />,
        meta: {
          title: '考试系统',
          parent: 'fabric'
        }
      },
    ]
  }
}