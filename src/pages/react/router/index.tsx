/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:46:10
 */
import { useStore } from '@react/hooks/useStore'
import React, { lazy, Suspense, useEffect, useState } from 'react'
import { Routes, Navigate, Link, Route } from 'react-router-dom';    //
import { Skeleton, Spin } from 'antd';
import CssRoute from './css'
import AntdRout from './antd'
import ThreeRout from './three'
import usefulTools from './usefulTools';
import fabricRoute from './fabric';

// 快速导入工具函数
export const lazyLoad = (moduleName: string) => {
  const Module = lazy(() => import(`@react/views/${moduleName}/index.tsx`))
  return (
    <Suspense fallback={<Spin />}>
      <Module />
    </Suspense>
  )
}
// 路由鉴权组件
export const Appraisal = (props: any) => {
  const token = useStore('token')
  // return token ? <SkeletonComponent>{props.children}</SkeletonComponent> : <Navigate to="/login" />
  return props.children

}


const routes = [

  {
    path: '',
    element: <Navigate to="login" />,

  },
  {
    path: '/',
    element: <Navigate to="login" />
  },
  {
    path: 'login',
    element: lazyLoad('login'),
    meta: {
      title: '登录'
    }
  },
  {
    path: '/layout',
    element: <Appraisal>{lazyLoad('layout')}</Appraisal>,
  },
  {
    path: 'home',
    element: <Appraisal>{lazyLoad('home')}</Appraisal>,
    meta: {
      title: '首页'
    }

  },

  {
    path: 'onlineEditor',
    element: <Appraisal>{lazyLoad('onlineEditor')}</Appraisal>,
    meta: {
      title: '在线编辑器'
    }

  },
  {
    path: 'mock',
    element: <Appraisal>{lazyLoad('mock')}</Appraisal>,
    meta: {
      title: 'mock'
    }
  },

  {
    path: 'form',
    element: <Appraisal>{lazyLoad('form')}</Appraisal>,
    meta: {
      title: '表格'
    }
  },
  {
    path: 'skeleton',
    element: lazyLoad('skeleton'),
    meta: {
      title: 'skeleton'
    }
  },

  {
    path: '*',
    element: lazyLoad('not-found'),
    meta: {
      title: '404'
    }
  },
  CssRoute(),
  AntdRout(),
  ThreeRout(),
  usefulTools(),
  fabricRoute()
]





export default routes



/**注册路由 */
export const registerRoutes = () => {
  return (
    <Routes>
      <Route path={'/'} key={1} element={lazyLoad('layout')} />
      <Route path={'/login'} key={2} element={lazyLoad('login')} />
      <Route path={'/home'} key={3} element={lazyLoad('home')} />
      <Route path={'/three'} key={4} element={lazyLoad('three')} >
        <Route path={'base'} key={5} element={lazyLoad('three/base')} />
        <Route path={'test'} key={6} element={lazyLoad('three/test')} />
      </Route>
      <Route path={'*'} key={7} element={lazyLoad('not-found')} />
    </Routes>
  )
}
