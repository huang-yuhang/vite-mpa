/*
 * @Description:
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:49:50
 */
import React,{ lazy } from 'react'

const Base = lazy(() => import('@react/views/three/components/base'))
const Cube = lazy(() => import('@react/views/three/components/cube'))

import { Appraisal, lazyLoad } from '..'

export default () => {
  return {
    path: 'three',
    element: <Appraisal>{lazyLoad('three')}</Appraisal>,
    children: [
      {
        path: 'base',
        element: <Base />,
        meta: {
          title: 'base',
          parent: 'three'
        }
      },
      {
        path: 'cube',
        element: <Cube />,
        meta: {
          title: '几何体',
          parent: 'three'
        }
      }
    ]
  }
}
