/*
* @Description: 
* @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:50:29
*/
import React,{ lazy, } from 'react'
import { Appraisal, lazyLoad } from '..'

const AudioRecord = lazy(() => import('@react/views/usefulTools/audioRecord'))
const ScreenRecord = lazy(() => import('@react/views/usefulTools/screenRecord'))


export default () => {
  return {
    path: 'usefulTools',
    element: <Appraisal>{lazyLoad('usefulTools')}</Appraisal>,
    meta: {
      title: '实用工具',
    },
    children: [
      {
        path: 'audioRecord',
        element: <AudioRecord />,
        meta: {
          title: '录音',
          parent: 'usefulTools'
        }
      },
      {
        path: 'screenRecord',
        element: <ScreenRecord />,
        meta: {
          title: '录屏',
          parent: 'usefulTools'
        }
      },
    ]
  }
}