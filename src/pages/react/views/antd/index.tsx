/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:16:41
 */
import React from 'react';
import { Outlet } from 'react-router-dom'

export default function Antd() {

  return <Outlet />

}
