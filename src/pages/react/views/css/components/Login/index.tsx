/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:09:34
 */
import React from 'react';

import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";
import { Form, Input, Checkbox, Button } from "antd";

const Login = () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        <div className={styles.box}>
          <div className={styles.left}>
            <h3 className={styles.blurBox}>welcome!</h3>
          </div>
          <div className={styles.right}>
            <h1>LOG IN</h1>
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              style={{ maxWidth: 600 }}
              initialValues={{ remember: true }}
              autoComplete="off"
            >
              <Form.Item
                label="email"
                name="email"
                rules={[{ required: true, message: '请输入邮箱!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: '请输入密码!' }]}
              >
                <Input.Password />
              </Form.Item>


              <div style={{
                display: 'flex',
                justifyContent: "space-between",
                margin: '5px'
              }}>
                <Checkbox>Remember me</Checkbox>
                <div
                  style={{
                    cursor: 'pointer',
                    color:'#6d6db0'
                  }}>forget?</div>
              </div>
              <Button htmlType="submit" block>
                登录
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </CodeDetail>
  );
};

export default Login;
