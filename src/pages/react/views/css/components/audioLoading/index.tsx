/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 13:43:21
 */
import React from "react";
import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";

const spanNum = 15;
const list = new Array(spanNum).fill(0);

const AudioLoading = () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        <div className={styles.content}>
          {list.map((item: number, index: number) => (
            <span className={styles[`span${index}`]} key={index} />
          ))}
        </div>
      </div>
    </CodeDetail>

  );
};

export default AudioLoading;
