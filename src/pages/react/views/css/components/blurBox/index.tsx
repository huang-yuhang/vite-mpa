/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:09:18
 */
import React from 'react';

import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";



export default () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents} >
      <div className={styles.container}>
        <div className={styles.card}>
          <h1>TiKi Platinum</h1>
          <h2>6228 8076 2232 8768</h2>
          <h3>van conf bb</h3>
          <h4>03 / 29</h4>
        </div>
      </div>
    </CodeDetail>


  )
}

