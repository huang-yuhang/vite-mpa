/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:09:23
 */
import React from 'react';

import CodeDetail from "@react/components/codeDetail";
import style from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";



export default () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents} >
      <div className={style.buttons}>
        <button className={style.customBtn + ' ' + style.btn}>Read More</button>
      </div>
    </CodeDetail>


  )
}

