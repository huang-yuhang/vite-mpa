/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:09:28
 */
import React from 'react';

import flower from "@react/assets/img/flower.jpg";
import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";
const Filter = () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        <div className={styles.left}>
          <span>
            变亮
          </span>
          <img src={flower} alt="" />
        </div>
        <div className={styles.right}>
          <span>
            变灰
          </span>
          <img src={flower} alt="" />
        </div>
      </div>
    </CodeDetail>

  );
};

export default Filter;
