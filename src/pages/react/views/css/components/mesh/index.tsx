import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";
import React from 'react';

const list = new Array(300).fill(0);

const Mesh = () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        {list.map((item: number, index: number) => (
          <div key={index} className={styles.item} />
        ))}
      </div>
    </CodeDetail>
  );
};

export default Mesh;
