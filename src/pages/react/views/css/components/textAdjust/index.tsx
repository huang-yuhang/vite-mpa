import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";
import React from 'react';

export default () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents} >
      <div className={styles.container}>
        <div className={styles.content}></div>
      </div>
    </CodeDetail>
  );
};