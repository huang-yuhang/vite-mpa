/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:10:00
 */
import React from 'react';

import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";

const TextShadow = () => {
  const { contents } = useCssData()

  const text = '这是一段文字'


  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        <div className={styles.content} data-text={text}>
          {text}
        </div>
      </div>
    </CodeDetail>

  );
};

export default TextShadow;
