/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 14:22:00
 */
import CodeDetail from "@react/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@react/hooks/useCss";
import React from 'react';

const list = new Array(4).fill(0);

const WarningLine = () => {
  const { contents } = useCssData()

  const renderTextList = () => (
    <span>
      Police line do not cross - Police line do not cross - Police line do not
      cross - Police line do not cross - Police line do not cross - Police line
      do not cross
    </span>
  );

  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        {list.map((item: number, index: number) => (
          <div className={styles.scroll} key={index}>
            {renderTextList()}
            {renderTextList()}
          </div>
        ))}
      </div>
    </CodeDetail>

  );
};

export default WarningLine;
