/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:10:32
 */
import React from 'react';

import { Outlet } from "react-router-dom";


export default () => <Outlet />