/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:10:27
 */

import React from 'react';

import { fabric } from 'fabric'
import { useEffect, useRef, useState } from 'react'
import testBg from '@react/assets/img/bg2.png'
import { addEvent, ICanvas, jsonData, colorList } from './utils'
import './index.scss'
import { AddThree, DeleteTwo, FileEditing } from '@icon-park/react'
import { message, Modal, Radio } from 'antd'

const Test = () => {

  const menuRef = useRef<any>(null)
  const activeEl = useRef<any>(null)

  let canvas: ICanvas

  /**隐藏菜单 */
  const hideMenu = () => menuRef.current.style = "display:none"


  const handleMenu = (type: 'add' | 'edit' | 'delete') => {
    console.log(`type ==>`, type);

    switch (type) {
      case 'add':
        setOpen(true)
        break;
      case 'edit':
        setOpen(true)
        break;

      case 'delete':
        // @ts-ignore
        const { option: { id } } = activeEl.current
        // 遍历所有对象并删除具有指定 id 的 polyline 对象
        canvas.getObjects().forEach(obj => {
          // @ts-ignore
          if (obj.type === 'polyline' && id === obj.id) {
            canvas.remove(obj)
            canvas.renderAll.bind(canvas)
          }
        });

        break;


      default:
        break;
    }


    return hideMenu()
  }

  const init = () => {
    canvas = new fabric.Canvas('can',
      {
        fireRightClick: true, // 启用右键，button的数字为3
        stopContextMenu: true, // 禁止默认右键菜单
        hoverCursor: "pointer",
        selection: false,
        width: window.innerWidth - 200,
        height: window.innerHeight
      })
    addEvent(canvas, hideMenu)

    canvas.setBackgroundImage(
      testBg,
      canvas.renderAll.bind(canvas),
      {
        scaleX: 0.6,
        scaleY: 0.6
      }
    )
    // 批量添加数据
    for (const item of jsonData.circle) {

      const circle = new fabric.Circle(item)

      const { point, option } = item.polyline
      const polyline = new fabric.Polyline(point, option);

      canvas.add(polyline)
      canvas.add(circle)
      circle.on('mousedown', (opt) => {
        const { button, } = opt
        // console.log(`opt ==>`, opt);
        if (button === 1 || button === 3) {

          // 设置右键菜单位置
          // 1. 获取菜单组件的宽高
          const menuWidth = menuRef.current.offsetWidth
          const menuHeight = menuRef.current.offsetHeight

          // 当前鼠标位置
          let pointX = opt.pointer!.x
          let pointY = opt.pointer!.y

          if (canvas.width! - pointX <= menuWidth) {
            pointX -= menuWidth
          }
          if (canvas.height! - pointY <= menuHeight) {
            pointY -= menuHeight
          }

          const style = `
          display:block;
          left: ${pointX}px;
          top: ${pointY}px;
          `
          // @ts-ignore
          activeEl.current = circle.polyline
          menuRef.current.style = style



        }

      })
    }

  }

  useEffect(() => { init() }, [])
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState();
  return (
    <div className='test'>
      <div id="menu" ref={menuRef}>
        <div onClick={() => handleMenu('add')}><AddThree theme="outline" size="24" fill="#333" /><span>新增</span></div>
        <div onClick={() => handleMenu('edit')}><FileEditing theme="outline" size="24" fill="#333" /><span>修改</span></div>
        <div onClick={() => handleMenu('delete')}><DeleteTwo theme="outline" size="24" fill="#333" /><span>删除</span></div>
      </div>
      <canvas id="can"></canvas>
      <Modal title="Basic Modal" open={open} onOk={() => setOpen(false)} onCancel={() => setOpen(false)}>
        <Radio.Group onChange={e => setValue(e.target.value)} value={value}>
          {colorList.map(item => {
            return <Radio key={item.value} value={item.value}>{item.name}</Radio>
          })}
        </Radio.Group>
      </Modal>
    </div>
  )
}

export default Test