export type ICanvas = fabric.Canvas & Partial<{
  isDragging: boolean,
  showMenu: boolean,
  lastPosX: number,
  lastPosY: number,
}>


/** */
export const addEvent = (canvas: ICanvas, hideMenu: Function) => {
  canvas.on('mouse:wheel', opt => {
    let delta = opt.e.deltaY // 滚轮，向上滚一下是 -100，向下滚一下是 100
    let zoom = canvas.getZoom() // 获取画布当前缩放值
    zoom *= 0.999 ** delta
    if (zoom > 20) zoom = 20
    if (zoom < 0.01) zoom = 0.01
    canvas.zoomToPoint({ // 关键点
      x: opt.e.offsetX,
      y: opt.e.offsetY
    },
      zoom
    )
    opt.e.preventDefault()
    opt.e.stopPropagation()
    hideMenu()

  })
  canvas.on('mouse:down', opt => { // 鼠标按下时触发
    let evt = opt.e
    // console.log(`evt ==>`, evt);
    hideMenu()
    if (evt.altKey === true || evt.ctrlKey === true) { // 是否按住alt
      canvas.isDragging = true // isDragging 是自定义的
      canvas.lastPosX = evt.clientX // lastPosX 是自定义的
      canvas.lastPosY = evt.clientY // lastPosY 是自定义的
    }
  })

  canvas.on('mouse:move', opt => { // 鼠标移动时触发
    if (canvas.isDragging) {
      let evt = opt.e
      let vpt = canvas.viewportTransform! // 聚焦视图的转换
      vpt[4] += evt.clientX - canvas.lastPosX!
      vpt[5] += evt.clientY - canvas.lastPosY!
      canvas.requestRenderAll()
      canvas.lastPosX = evt.clientX
      canvas.lastPosY = evt.clientY
    }
  })

  canvas.on('mouse:up', opt => { // 鼠标松开时触发
    canvas.setViewportTransform(canvas.viewportTransform!) // 设置此画布实例的视口转换  
    canvas.isDragging = false
  })
}


export const colorList = [
  {
    name: '红色',
    color: '#FF3939',
    value: 1
  },
  {
    name: '黑色',
    color: '#000000',
    value: 2

  },
  {
    name: '黄色',
    color: '#FFB81F',
    value: 3
  },
  {
    name: '蓝色',
    color: '#297FDF',
    value: 4
  }
]



export const jsonData = {
  circle: [
    {
      radius: 4,
      fill: 'orange',
      left: 458,
      top: 253,
      stroke: 'red',
      strokeWidth: 1.5,
      selectable: false,
      hasControls: false,
      polyline: {
        point: [
          { x: 463, y: 253 },
          { x: 463, y: 225 },
          { x: 510, y: 225 },
        ],
        option: {
          fill: 'transparent',
          stroke: colorList[0].color,
          strokeWidth: 3,
          strokeLineCap: 'round',
          strokeLineJoin: 'round',
          selectable: false,
          hasControls: false,
          id: 2333
        },
      }
    },
    {
      radius: 4,
      fill: 'orange',
      left: 470,
      top: 253,
      stroke: 'red',
      strokeWidth: 1.5,
      selectable: false,
      hasControls: false,
      polyline: {
        point: [
          { x: 475, y: 253 },
          { x: 475, y: 225 },
          { x: 510, y: 225 },
        ],
        option: {
          fill: 'transparent',
          stroke: colorList[1].color,
          strokeWidth: 3,
          strokeLineCap: 'round',
          strokeLineJoin: 'round',
          selectable: false,
          hasControls: false,
          id: 2334
        },
      }
    },
    {
      radius: 4,
      fill: 'orange',
      left: 481,
      top: 253,
      stroke: 'red',
      strokeWidth: 1.5,
      selectable: false,
      hasControls: false,
      polyline: {
        point: [
          { x: 486, y: 253 },
          { x: 486, y: 225 },
          { x: 510, y: 225 },
        ],
        option: {
          fill: 'transparent',
          stroke: colorList[2].color,
          strokeWidth: 3,
          strokeLineCap: 'round',
          strokeLineJoin: 'round',
          selectable: false,
          hasControls: false,
          id: 2335
        },
      }
    },
    {
      radius: 4,
      fill: 'orange',
      left: 492,
      top: 253,
      stroke: 'red',
      strokeWidth: 1.5,
      selectable: false,
      hasControls: false,
      polyline: {
        point: [
          { x: 497, y: 253 },
          { x: 497, y: 225 },
          { x: 510, y: 225 },
        ],
        option: {
          fill: 'transparent',
          stroke: colorList[3].color,
          strokeWidth: 3,
          strokeLineCap: 'round',
          strokeLineJoin: 'round',
          selectable: false,
          hasControls: false,
          id: 2336
        },
      }
    },
  ]
}