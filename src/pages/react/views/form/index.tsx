/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-24 11:49:44
 */
import React, { useEffect, useRef, useState } from 'react';
import { Button, Cascader, Checkbox, DatePicker, Form, Input, InputNumber, Radio, Select, Switch, TreeSelect, Upload } from 'antd';
import type { FormInstance } from 'antd/es/form';
import TextArea from 'antd/es/input/TextArea';
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';
import { PlusOutlined } from '@ant-design/icons';
import Card from 'antd/es/card/Card';

const { RangePicker } = DatePicker;
const App: React.FC = () => {
  const [form] = Form.useForm();
  const formRef = useRef<FormInstance>(null);
  const [componentDisabled, setComponentDisabled] = useState<boolean>(false);
  const [fileList, setFileList] = useState<UploadFile[]>([]);

  useEffect(() => {
    if (formRef.current) {
      form.setFieldValue('switch', false)
      form.setFieldValue('upload', [{
        uid: '-1',
        name: 'image.png',
        status: 'done',
        url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      },])
    }
  }, [form])

  return (
    <>
      <Card>
        <Checkbox
          checked={componentDisabled}
          onChange={(e) => setComponentDisabled(e.target.checked)}
        >
          disabled Form
        </Checkbox>
        <Form
          form={form}
          ref={formRef}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          disabled={componentDisabled}
        // style={{ maxWidth: 600 }}
        >
          <Form.Item label="Checkbox" name="disabled" valuePropName="checked">
            <Checkbox>Checkbox</Checkbox>
          </Form.Item>
          <Form.Item label="Radio" name='Radio'>
            <Radio.Group>
              <Radio value="apple"> Apple </Radio>
              <Radio value="pear"> Pear </Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="Switch" valuePropName="checked" initialValue={true} name='switch'>
            <Switch />
          </Form.Item>
          <Form.Item
            label="Upload"
            valuePropName="fileList"
            // 如果没有下面这一句会报错
            getValueFromEvent={e => {
              if (Array.isArray(e)) {
                return e;
              }
              return e && e.fileList;
            }}
            name='upload'
          >
            <Upload.Dragger
              fileList={fileList}
              multiple
              listType="picture-card"
              action={import.meta.env.VITE_BASE_URL + '/upload'}
            >
              <div>
                <PlusOutlined style={{ fontSize: '22px', color: '#767474' }} />
              </div>
            </Upload.Dragger>
          </Form.Item>
          <Form.Item  >
            <Button htmlType='submit' onClick={() => {
              console.log(form.getFieldsValue());
            }}>Submit</Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
};

export default App;