/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-07 09:13:51
 */

import JSONFormatter from "@react/components/JSONFormatter";
import { useFullScreen } from "@react/hooks/useFullScreen";
import { Button, Modal, Skeleton, Space, TreeSelect } from "antd";
import { useEffect, useRef, useState } from "react";
import modal from './modal.module.scss'
import React from 'react';


const Home = () => {
  const [src, setSrc] = useState({})
  const [open, setOpen] = useState(false);
  const [active, setActive] = useState(true) as any;
  const [block, setBlock] = useState(false) as any;
  const [size, setSize] = useState('default') as any;
  const [buttonShape, setButtonShape] = useState('default') as any;
  const [avatarShape, setAvatarShape] = useState('circle') as any;

  const ref = useRef(null)

  const getDate = () => {
    http.post<any>('/test/getFileTree').then(res => {
      console.log(`res.data ==>`, res.data);
      setSrc(res.data)
      setOpen(true);
    })

  }

  return (
    <div ref={ref} style={{ background: '#fff' }}>
      <Modal
        className={modal.custom_modal}
        title={'生成的数据'}
        width={1000}
        open={open}
        onOk={() => setOpen(false)}
        onCancel={() => setOpen(false)}
        okText={'确认'}
        cancelText={'取消'}
        centered
      >
        <JSONFormatter src={src} theme={'twilight'} />
      </Modal>
      <Space>
        <Skeleton.Button active={active} size={size} shape={buttonShape} block={block} />
        <Skeleton.Avatar active={active} size={size} shape={avatarShape} />
        <Skeleton.Input active={active} size={size} />
      </Space>
      <br />
      <br />
      <div>
        <Button onClick={getDate}>获取数据</Button>
        <Button onClick={() => useFullScreen(ref.current)}>全屏</Button>
      </div>
    </div >
  );
};

export default Home;