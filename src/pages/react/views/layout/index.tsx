/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:52:38
 */
import React from'react'
import { Layout as AntLayout, } from 'antd';
const { Content, } = AntLayout;
import { Outlet, useRoutes, } from 'react-router-dom'
import routes from '@react/router';
import Sider from './components/Sider';

export default function Layout() {

  return (
    <AntLayout style={{ height: '100vh' }}>
      <Sider />
      <Content style={{ overflow: "auto" }}>
        {useRoutes(routes)}
        <Outlet />
      </Content>
    </AntLayout>
  )
}
