import { useEffect, useState } from "react";
import ReactJson from 'react-json-view'
import React from 'react';

export default function Mock() {

  const [value, setValue] = useState({})


  useEffect(() => {
    const data = mock({
      'data|10': [
        {
          id: Random.id(), //360000201812263950
          'flag|1-2': Random.boolean(), //随机true或false
          'price|10-50.2-5': 1, //10-50随机数 小数点2-5位
          'star|1-5': '⭐', //把字符串重复1-5次
          url: Random.url(),
          email: Random.email(),
          address: Random.county(true), //省市区地址
          date: Random.date(), //随机日期  1987-12-16
          time: Random.time(), //随机时间  20:49:32
          name: Random.cname(), // 随机中文名  田丽
          pic: Random.dataImage('200x200', '我嫩爹'), //data:image/png;base64,iVBORw0KGgoA ....
          description: Random.cparagraph(1, 3), // 段落1-3行  "技根走知政就存图使对际积确。带当共活速例选称积应克话支往。"
          title: Random.ctitle(8, 12), //音美直身日花他消入
          'age|10-40': 1, // 15
          domain: Random.domain()
        }
      ]
    })


    setValue(data)

  }, [])


  return (
    <div style={{ width: '100%', height: 'calc(100vh)', overflow: 'scroll' }}>

      <ReactJson
        src={value}
        // 子节点数量
        displayObjectSize={false}
        // 子节点类型.
        displayDataTypes={false}
        theme="google"
        iconStyle="triangle"
        collapseStringsAfterLength={30}
        style={{ fontSize: 16, fontWeight: 'bold' }}
        indentWidth={2}
      />
    </div>
  )
}
