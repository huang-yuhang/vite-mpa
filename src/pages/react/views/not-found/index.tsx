/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:14:31
 */
import React from 'react';
import { useLocation, useNavigate, } from 'react-router-dom';
import imgSrc from '@react/assets/img/404.png'
import './index.scss'

export default function NotFound() {
  const { pathname } = useLocation()
  const navigate = useNavigate()
  return (
    <div className='page-404'>
      <img className='img-404' src={imgSrc} alt="" />
      <div style={{ textAlign: 'center' }}>未找到此 <a style={{ color: 'green' }}>{pathname}</a> 路由!</div>
      <div style={{ textAlign: 'center', fontSize: '18px', cursor: 'pointer' }} onClick={() => navigate('home', { replace: true })}>返回</div>
    </div>
  )
}
