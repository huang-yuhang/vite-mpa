/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 17:17:07
 */
import { Avatar, Divider, List, Skeleton, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
type IDataSource = {
  id: number
  title: string
  avatar: string
  href: string
  description: string
  content: string
}
const App: React.FC = () => {
  const [loading, setLoading] = useState(true);
  const [dataSource, setDataSource] = useState<IDataSource[]>([]);

  const loadMore = () => {
    http.post<IDataSource[]>('/test').then(res => {
      setDataSource([...dataSource, ...res.data])
    })
  }
  useEffect(() => {
    http.post<IDataSource[]>('/test').then(res => {
      setDataSource(res.data)
      setLoading(false)
    })
  }, [])

  return (
    <div id="scrollableDiv"
      style={{
        height: 800,
        overflow: 'auto',
        padding: '10px 16px',
      }}
    >
      <Skeleton loading={loading} active avatar>
        <InfiniteScroll
          dataLength={dataSource.length}
          next={loadMore}
          hasMore={dataSource.length < 100}
          loader={<Skeleton avatar paragraph={{ rows: 10 }} active />}
          endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
          scrollableTarget="scrollableDiv"
        >
          <List
            itemLayout="vertical"
            size="large"
            dataSource={dataSource}
            renderItem={(item) => {
              return (
                <List.Item key={item.id}>
                  <List.Item.Meta
                    avatar={<Avatar src={item.avatar} />}
                    title={<a href={item.href}>{item.title}</a>}
                    description={item.description}
                  />
                  {item.content}
                </List.Item>
              )
            }}
          />
        </InfiniteScroll>
      </Skeleton>
    </div>
  );
};


export default App;