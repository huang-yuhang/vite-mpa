/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-09 14:46:30
 */
import React from 'react';

import { useEffect, useState } from 'react';
import { Outlet, useRoutes } from 'react-router-dom'
import ThreeRoute from '@react/router/three';
export default function Three() {


  return (
    <div id="base">
      <Outlet />
    </div>
  )
}
