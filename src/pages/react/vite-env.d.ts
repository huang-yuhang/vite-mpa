/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 17:00:45
 */
/// <reference types="vite/client" />
import { IMock, IRandom, ISleep } from '@react/config/globalInject'
import { IHttp } from '@react/http/index'
declare global {
  const Random: IRandom
  const sleep: ISleep
  const mock: IMock;
  const http: IHttp;

  /**注意 定义Window接口 首字母一定要大写 否则会出问题 */
  interface Window {
    mock: IMock;
    Random: IRandom
    sleep: ISleep
    http: IHttp
  }

}