/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-27 18:56:56
 */
import { createApp } from 'vue'
import '@/assets/css/base.css'
import router from './router/index'
import App from './App.vue'
createApp(App)
  .use(router)
  .mount('#vue')

