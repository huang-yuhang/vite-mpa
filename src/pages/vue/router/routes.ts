/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-27 18:57:11
 */

import { RouteRecordRaw } from 'vue-router'

const { DEV, PROD } = import.meta.env
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: {
      name: 'welcome',
      query: {
        appName: 'vue',
      }
    }
  },
  {
    path: '/welcome',
    name: 'welcome',
    component: () => import('@/views/welcome/index.vue'),
    meta: {
      title: '欢迎'
    }
  },
  {
    path: '/layout',
    name: 'layout',
    component: () => import('@/views/layout/pageLayout/index.vue'),
    redirect: {
      name: 'api',
    },
    children: [
      {
        path: 'api',
        name: 'api',
        component: () => import('@/pages/koa/views/api/index.vue'),
        meta: {
          title: '接口'
        }
      },
      {
        path: 'db',
        name: 'db',
        component: () => import('@/pages/koa/views/db/index.vue'),
        meta: {
          title: '数据库'
        }
      },
      {
        path: 'onlineEditor',
        name: 'onlineEditor',
        component: () => import('@/pages/koa/views/onlineEditor/index.vue'),
        meta: {
          title: '在线编辑'
        }
      }
    ]
  },
  {
    path: '/:pathMatch(.*)',
    name: '404',
    component: () => import('@/views/error/404.vue'),
    meta: {
      title: '404'
    }
  }

]
if (DEV) {
  routes[2].children.push({
    path: 'dev',
    name: 'dev',
    component: () => import('@/views/dev/index.vue'),
    meta: {
      title: '开发'
    }
  })
}
export default routes