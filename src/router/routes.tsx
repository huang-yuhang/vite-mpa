/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-08 20:28:33
 */

import { RouteRecordRaw } from 'vue-router'

const { DEV, PROD } = import.meta.env
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: {
      name: 'login',
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: 'login'
    }
  },
  {
    path: '/resetPwd',
    name: 'resetPwd',
    component: () => import('@/views/login/resetPwd.vue'),
    meta: {
      title: '重置密码'
    }
  },
  {
    path: '/regist',
    name: 'regist',
    component: () => import('@/views/login/regist.vue'),
    meta: {
      title: '注册'
    }
  },
  {
    path: '/welcome',
    name: 'welcome',
    component: () => import('@/views/welcome/index.vue'),
    meta: {
      title: '欢迎'
    }
  },
  {
    path: '/layout',
    name: 'layout',
    redirect: DEV ? '/layout/dev' : '/layout/home',
    component: () => import('@/views/layout/mainLayout/index.vue'),
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('@/views/home/index.vue'),
        meta: {
          title: '首页'
        }
      },
    ]
  },
  {
    path: '/:pathMatch(.*)',
    name: '404',
    component: () => import('@/views/error/404.vue'),
    meta: {
      title: '404'
    }
  }

]
// 开发环境添加路由
if (DEV) {
  routes.forEach(item => {
    if (item.name === 'layout') {
      item.children.push({
        path: 'dev',
        name: 'dev',
        component: () => import('@/views/dev/index.vue'),
        meta: {
          title: '开发'
        }
      })
    }
  })

}
export default routes