/*
 * @Description: 使用 pinia && 持久化插件
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-27 20:29:09
 */
import piniaPersist from 'pinia-plugin-persist'
import { createPinia } from 'pinia'
import { App, Plugin } from 'vue'
const pinia: Plugin = (app: App) => {
  const plugin = createPinia().use(piniaPersist)
  app.use(plugin)
}
export default pinia
