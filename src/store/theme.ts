/*
 * @Description:userStore
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-27 20:47:17
 */
import { defineStore, } from 'pinia'

export const themeStore = defineStore('themeStore', {
  /** state==>必须是一个方法 && 返回值为对象 */
  state: () => {
    return {
      theme: ''
    }
  },
  actions: {
    /**设置路由 */
    setTheme(theme: string) {
      this.theme = theme
      document.documentElement.setAttribute('data-theme', theme)
    }
  },
  /**pinia持久化方式 */
  persist: {
    enabled: true, //是否启用 "persist" 插件持久化储存
    /**策略 （配置） */
    strategies: [
      {
        key: 'themeStore', //在 localStorage 或 sessionStorage 中国存储的 "key" 值
        storage: localStorage //存储方式 localStorage | sessionStorage  (默认是存储在sessionStorage的)
      }
      /*****************************持久化保留(某个字段)*******************************/
      // { storage: sessionStorage, paths: ['name', 'age'] }, //保留 name age 到 sessionStorage
      // { storage: localStorage, paths: ['sex'] } //保留 sex  到 localStorage
      /*****************************持久化保留(某个字段)end*******************************/
    ]
  }
})
