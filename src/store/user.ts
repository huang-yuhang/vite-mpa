/*
 * @Description:userStore
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-07 17:10:29
 */
import { UserModel } from '@/modal/user'
import { defineStore, } from 'pinia'
import { constants } from './constant'
interface UserInfo {
  id: string
  token: string
  name: string
  role: string
  email: string
}
export const rootUserStore = defineStore(constants.ROOT_USER_STORE, {
  /** state==>必须是一个方法 && 返回值为对象 */
  state: () => {
    return {
      id: '',
      email: '',
      username: '',
      address: '',
      token: '',
    } as UserModel
  },
  actions: {
    /**设置路由 */
    setInfo(info: UserModel) {
      Object.keys(info).forEach(key => this[key] = info[key])
    }
  },
  /**pinia持久化方式 */
  persist: {
    enabled: true, //是否启用 "persist" 插件持久化储存
    /**策略 （配置） */
    strategies: [
      {
        key: 'rootUserStore', //在 localStorage 或 sessionStorage 中国存储的 "key" 值
        storage: localStorage //存储方式 localStorage | sessionStorage  (默认是存储在sessionStorage的)
      }
      /*****************************持久化保留(某个字段)*******************************/
      // { storage: sessionStorage, paths: ['name', 'age'] }, //保留 name age 到 sessionStorage
      // { storage: localStorage, paths: ['sex'] } //保留 sex  到 localStorage
      /*****************************持久化保留(某个字段)end*******************************/
    ]
  }
})
