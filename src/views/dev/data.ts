/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-27 18:26:22
 */
import { useViteFile } from '@/hooks/useViteFile'
export const devPages = [
  {
    name: 'daisyui',
    url: 'https://daisyui.com/components/',
    des: 'Use Tailwind CSS but write fewer class names',
    icon: useViteFile('dev/daysyUI.svg')
  },
  {
    name: 'tailwind.css',
    url: 'https://tailwindui.com/',
    des: 'Build your next idea even faster.',
    icon: useViteFile('dev/tailwind.png')
  },
  {
    name: 'elementPlus',
    url: 'https://element-plus.gitee.io/zh-CN/component/button.html',
    des: '一套为开发者、设计师和产品经理准备的基于 Vue 3.0 的桌面端组件库',
    icon: useViteFile('dev/element-plus.svg')
  },
  {
    name: 'antd',
    url: 'https://ant.design/index-cn',
    des: '助力设计开发者「更灵活」地搭建出「更美」的产品，让用户「快乐工作」～  ',
    icon: useViteFile('dev/antd.svg')
  },
  {
    name: 'vite',
    url: 'https://cn.vitejs.dev/',
    des: '下一代的前端工具链',
    icon: useViteFile('dev/vite.png')
  },
  {
    name: 'skill-icon',
    url: 'https://github.com/tandpfun/skill-icons',
    des: 'Showcase your skills on your GitHub or resumé with ease!',
    icon: useViteFile('dev/skill-icon.png')
  },
  {
    name: '即时工具',
    des: '致力打造安全、快捷、好用的在线工具箱~',
    url: 'https://www.67tool.com/',
    icon: useViteFile('dev/js-utils.svg')
  },
  {
    name: 'vue',
    des: '渐进式JavaScript 框架',
    url: 'https://cn.vuejs.org/',
    icon: useViteFile('dev/vue.svg')
  },
  {
    name: 'pinia',
    des: '您将喜欢使用的 Vue 存储库',
    url: 'https://pinia.web3doc.top/',
    icon: useViteFile('dev/pinia.png')
  },
  {
    name: 'iconpark',
    des: '丰富多彩的资源库免费使用',
    url: 'http://iconpark.oceanengine.com/official',
    icon: useViteFile('dev/icon-park.svg')
  }
]