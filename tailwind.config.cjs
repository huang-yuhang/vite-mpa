/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-27 20:48:43
 */
module.exports = {
  content: ['./src/**/*.{vue,js,ts,tsx}', './index.html'],
  plugins: [require('daisyui'), require('@tailwindcss/forms'),],
  themes: [ "dark", "light"],
};
