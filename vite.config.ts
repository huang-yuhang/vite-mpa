// 类型提示
import { defineConfig, loadEnv } from 'vite'
import { resolve } from 'path'
import { buildPrompt, outDir } from './vite/build'
import { getPages } from './vite/dev'
import { usePlugin } from './vite/plugins'
// config
export default defineConfig(async ({ command, mode, ssrBuild }) => {
  const baseUrl = loadEnv(mode, process.cwd()).VITE_BASE_URL
  console.log({ baseUrl, command, mode, ssrBuild });
  let input: any
  getPages()
  if (command === 'build') {
    input = await buildPrompt()
    console.log(`input ==>`, input);
  }
  return {
    // 生产或开发环境下的基础路径
    base: '/',
    // 需要用到的插件数组
    plugins: usePlugin(),
    // 静态资源服务目录地址
    publicDir: resolve(__dirname, 'public'),
    // 存储缓存文件的目录地址
    cacheDir: '',
    //
    resolve: {
      // 设置文件目录别名
      alias: {
        '@': resolve(__dirname, './src'),
        '@css': resolve(__dirname, './src/assets/css'),
        '@react': resolve(__dirname, './src/pages/react'),
      },
      //
    },
    //
    css: {
      // postcss-modules 行为配置
      modules: {
        // ...
      },
      // 传递给css预处理器的配置项
      preprocessorOptions: {
        // 指定less预处理的配置项
        less: {
          // ...
        },
      },
    },
    // esbuild 选项转换配置
    esbuild: {
      // ...
      // 在react组件中无需导入react
      // jsxInject: `import React from 'react'`,
      // vue 使用jsx
      jsxFactory: 'h',
      jsxFragment: 'Fragment',
    },
    // 静态资源处理
    assetsInclude: '',
    // 开发服务器选项
    server: {
      // proxy: {
      //   '/api': {
      //     target: baseUrl,
      //     changeOrigin: true,
      //     rewrite: (path) => path.replace(/^\/api/, '') // 不可以省略rewrite
      //   }
      // }

    },
    // 构建配置项
    build: {
      modulePreload: { //https://cn.vitejs.dev/config/build-options.html
        polyfill: false,
      },
      // 指定输出目录
      outDir: outDir(mode),
      // 指定静态资源存放目录
      assetsDir: 'assets',
      // 启用、禁用css代码拆分
      cssCodeSplit: true,
      // 构建是否生成source map文件
      sourcemap: false,
      // rollup 配置打包项
      rollupOptions: {
        input,
        output: {
          chunkFileNames: 'static/lib/[name]-[hash].js',
          entryFileNames: 'static/pages/[name]/app_[name]-[hash].js',
          assetFileNames: 'static/assets/[ext]/[name]-[hash].[ext]',
          // manualChunks(id) {
          //   //静态资源分拆打包
          //   if (id.includes('node_modules')) {
          //     return id.toString().split('node_modules/')[1].split('/')[0].toString()
          //   }
          // }
          // 将第三方依赖库单独打包成一个文件
          manualChunks: {
            react: ['react', 'react-dom'],
            vue: ['vue', 'vue-router', 'pinia'],
          }
        },

      },
      // 构建目录自动清除
      emptyOutDir: true,
      target: 'modules',
    },
    // 依赖优化配置项
    optimizeDeps: {
      // 依赖构建入口
      entries: '',
      // 排除不需要构建的依赖项
      exclude: [],
      // 只打包使用到的第三方依赖库
      include: ['react', 'react-dom',]
    },
  }
})
