/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-06 16:56:54
 */
import fs from 'fs'
import path from 'path'
import { createPromptModule } from "inquirer";

/** 获取项目列表*/
export const getProjects = () => {
  const projectPath = path.join(__dirname, '../src/pages')
  return new Promise<string[]>((resolve, reject) => {
    const projects = fs.readdirSync(projectPath)
    resolve(projects)
  })
}

/**
 * 打包提示
 * @returns 入口文件
 */
export const buildPrompt = async () => {
  const projects = await getProjects()
  const prompt = createPromptModule();
  let input = {} as any
  const { entry } = await prompt([
    {
      type: "checkbox",//多选
      name: "entry",
      message: "请选择打包的页面",
      checked: true,
      choices: projects,
    },
  ]);
  if (entry.length == 0) {
    projects.map(item => {
      input[item] = `html/${item}.html`
    })
  } else {
    entry.map((item: string) => input[item] = `html/${item}.html`)
  }
  const { entry: root } = await prompt([
    {
      type: "list",
      name: "entry",
      message: "是否打包根项目",
      choices: ['是', '否'],
    },
  ]);
  if (root == '是') {
    input.index = 'index.html'
  }
  return input
}


/** */
export const outDir = (mode: string) => {
  // const buildPath = mode === 'dev' ? 'C:/Users/admin/Desktop/nginx-1.23.2/html' : path.join(__dirname, '../dist')
  const buildPath = path.join(__dirname, '../dist')
  console.log(`buildPath ==>`, buildPath);
  return buildPath
}