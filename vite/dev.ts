/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-05-26 18:22:34
 */
import fs from 'fs'
import path from 'path'

/**获取多页面数据（有哪些页面） 创建 pages.json */
export const getPages = () => {
  const pageDir = path.join(__dirname, '../src/pages')
  const pageJson = path.join(__dirname, '../src/pages.json')
  const pages = fs.readdirSync(pageDir)
  const data = pages.map(item => ({ name: item }))
  fs.writeFileSync(pageJson, JSON.stringify(data))
}