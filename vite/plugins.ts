/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-06-12 17:15:45
 */
import { visualizer } from 'rollup-plugin-visualizer';
import vue from '@vitejs/plugin-vue'
import vueJsx from "@vitejs/plugin-vue-jsx";
import react from '@vitejs/plugin-react'
import { PluginOption } from 'vite';
import AutoImport from 'unplugin-auto-import/vite'
import { ElementPlusResolver, AntDesignVueResolver } from 'unplugin-vue-components/resolvers'
import Components from 'unplugin-vue-components/vite'
/**
 * 压缩 gzip
 */
import viteCompression from 'vite-plugin-compression'
/** */
export const usePlugin = (): PluginOption[] => {
  return [
    vue(),
    // vueJsx(),
    react({
      babel: {
        plugins: ['@babel/plugin-transform-react-jsx'],
        // // Use .babelrc files
        // babelrc: true,
        // // Use babel.config.js files
        // configFile: true,
      },
    }),
    AutoImport({
      imports: ['vue', 'vue-router'], // 自动导入vue和vue-router相关函数
      dts: `@types/auto-import-vue.d.ts`, // 生成 `auto-import.d.ts` 全局声明
    }),
    // ...
    AutoImport({
      resolvers: [ElementPlusResolver(),AntDesignVueResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver(), AntDesignVueResolver()],
    }),
    // brotli 压缩
    // viteCompression({
    //   disable: true, //是否禁用
    //   threshold: 10240,
    //   algorithm: 'brotliCompress',
    //   ext: '.br'
    // }),
    // gzip压缩
    viteCompression({
      disable: false, //是否禁用
      threshold: 10240,// 10 kb的压缩
      algorithm: 'gzip',
      ext: '.gz'
    }),
    visualizer(),

  ]
}